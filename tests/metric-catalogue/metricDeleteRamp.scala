package specs.metriccatalogue
import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._


class deleteMetricsRamp extends Simulation {

val conf = ConfigFactory.load()
	val baseURL= conf.getString("baseURL")
   val baseUser = conf.getString("numUsers")
   val numUsers = baseUser.toInt
   val baseRamp = conf.getString("rampLength")
   val rampLength = baseRamp.toInt

	val httpProtocol = http
		.baseURL(baseURL)
		.inferHtmlResources()



    val uri1 = baseURL

	/*val scn = scenario("Get SLAs")
		.exec(http("getList")
			.get("/sla-manager/cloud-sla/slas"))
		.pause(6)
		.exec(http("getSLA")
			.get("/sla-manager/cloud-sla/slas/56A743FBE4B08C6B6B88DA64"))*/
	val users = scenario("Get Security Mechanisms").exec(Delete.deleteMetrics)

	setUp(users.inject(rampUsers(numUsers) over(rampLength seconds)).protocols(httpProtocol))


object Delete {


	val deleteMetrics = exec(http("Get ALL Security Metrics")
    		.get("/metric-catalogue/cloud-sla/security-metrics/")
    		.check(regex("""security-metrics\/\/(.{15,22}?)"[,]{0,1}""").findAll.saveAs("listMetrics")))
			.exec(http("Delete Security Metric")
    		.delete("/metric-catalogue/cloud-sla/security-metrics/${listMetrics.random()}"))


}

 
}