package specs.metriccatalogue

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import java.util.concurrent.ThreadLocalRandom
import com.typesafe.config._


class uploadMetricInject extends Simulation {


val conf = ConfigFactory.load()
	val baseURL= conf.getString("baseURL")
   val baseUser = conf.getString("numUsers")
   val numUsers = baseUser.toInt

	val httpProtocol = http
		.baseURL(baseURL)
		.inferHtmlResources()



    val uri1 = baseURL

	/*val scn = scenario("Get SLAs")
		.exec(http("getList")
			.get("/sla-manager/cloud-sla/slas"))
		.pause(6)
		.exec(http("getSLA")
			.get("/sla-manager/cloud-sla/slas/56A743FBE4B08C6B6B88DA64"))*/
	
	val users = scenario("Upload Services").exec(Upload.uploadmetric)

	
	setUp(users.inject(atOnceUsers(numUsers)).protocols(httpProtocol))

object Upload{


	val uploadmetric = 

	// con la session riesco effettivamente a chiamare più volte il metodo getRandom
	exec { session =>
 
 	 session.set("random", RandomGen.randomInt())
	}
	 .exec(http("Upload Security Metric")
			.post("/metric-catalogue/cloud-sla/security-metrics" )
			.body(StringBody("""{"referenceId":"metric_example"""+"${random}"+"""","XMLdescription":"<?xml version=\"1.0\" encoding=\"UTF-8\"?><specs:AbstractMetric xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:specs=\"http://specs-project.eu/schemas/metrics\" xsi:schemaLocation=\"http://specs-project.eu/schemas/metrics metrics.xsd\" name=\"Personal_data_disclosure\" referenceId=\"data_disclosure\"><specs:AbstractMetricDefinition><specs:unit name=\"\"><specs:enumUnit><enumItemsType>String</enumItemsType><enumItems><enumItem><value>none</value><description>no disclosure at all</description></enumItem><enumItem><value>limited</value><description>only limited disclosure: no correlation with owner</description></enumItem><enumItem><value>complete</value><description>disclosure and correlation</description></enumItem></enumItems></specs:enumUnit></specs:unit><specs:scale><specs:Qualitative>Nominal</specs:Qualitative></specs:scale><specs:expression>This metric can be measured by installing an Agent on the storage site that detects undesired accesses to sensitive data</specs:expression><specs:definition>This metric expresses the capability of a provider of correlating hosted sensitive information to people they belong to </specs:definition><specs:note></specs:note></specs:AbstractMetricDefinition><specs:AbstractMetricRuleDefinition><specs:RuleDefinition><specs:definition/><specs:note/></specs:RuleDefinition></specs:AbstractMetricRuleDefinition><specs:AbstractMetricParameterDefinition><specs:ParameterDefinition><specs:definition/><specs:note/><specs:parameterType/></specs:ParameterDefinition></specs:AbstractMetricParameterDefinition><specs:UnderlyingAbstractMetric/></specs:AbstractMetric>","metricType":"abstract"}"""))
			)

	
		
}

object RandomGen{
	def randomInt() : Int = {
      return ThreadLocalRandom.current.nextInt(10000)
   }
}


}