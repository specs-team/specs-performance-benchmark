package specs.planning

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._
import specs.planning.Planning._

class SupplyChainFlowAtOnce extends Simulation {
	val conf = ConfigFactory.load()
	val baseURL = conf.getString("baseURL")
	val baseUser = conf.getString("numUsers")
	val numUsers = baseUser.toInt
	val httpProtocol = http.baseURL(baseURL).inferHtmlResources()

	val planning = scenario("Planning supply chain flow, " + baseUser + " users at once").exec(
		Planning.createSLA, Planning.createSCA, Planning.getSCASC
	)
	setUp(planning.inject(atOnceUsers(numUsers)).protocols(httpProtocol))
}
