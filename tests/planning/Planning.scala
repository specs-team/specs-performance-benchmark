package specs.planning

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._

object Planning {
	val createSLA = exec(http("Create SLA")
		.post("/sla-manager/cloud-sla/slas")
		.body(RawFileBody("../user-files/data/SLATemplate-WebPool-SVA.xml"))
		.check(regex("""(.*)""").saveAs("SLA_ID"))
		.check(status.is(session => 201))
	)

	val createSCA = exec(http("Create SCA")
		.post("/planning-api/sc-activities")
		.header("Content-Type", "application/json")
		.body(ELFileBody("../user-files/data/sca-input-data.json"))
		.check(jsonPath("$.id")
		.find(0).saveAs("SUPPLY_CHAIN_ACTIVITY_ID"))
		.check(status.is(session => 201))
	)

	val getSupplyChains = exec(http("GetSupplyChain")
		.get("/planning-api/sc-activities/${SUPPLY_CHAIN_ACTIVITY_ID}/supply-chains")
		.check(jsonPath("$[0].id")
		.find(0).saveAs("SUPPLY_CHAIN_ID"))
		.check(status.is(session => 200))
	)

	val createPlanningActivity = exec(http("Create planning activity")
        .post("/planning-api/plan-activities")
        .header("Content-Type", "application/json")
        .body(ELFileBody("../user-files/data/planning-activity.json"))
        .check(status.is(session => 201))
    )

	val getSCASC = exec(http("Get SCA supply chains")
		.get("/planning-api/sc-activities/${SUPPLY_CHAIN_ACTIVITY_ID}/supply-chains")
		.check(jsonPath("$[1].securityMechanisms[1]").find(0).is("SM2"))
		.check(jsonPath("$[1].cloudResource.providerId").find(0).is("ec2"))
		.check(status.is(session => 200))
	)

	val getSCAIDs = exec(http("Get SC activities ID")
		.get("/planning-api/sc-activities")
		.check(jsonPath("$.item_list[*].id")
		.findAll.saveAs("scaID"))
		.check(status.is(session => 200))
	)

	val selectSCA = repeat(20){
		exec(http("Select SCA")
		.get("/planning-api/sc-activities/${scaID.random()}")
		.check(status.is(session => 200))
	)}

	val checkSCAStatus = repeat(20){
		exec(http("Check SCA status")
		.get("/planning-api/sc-activities/${scaID.random()}/state")
		.check(status.is(session => 200))
		)}

	val getSCASupplyChains = repeat(20){
		exec(http("Get SCA supply chains")
		.get("/planning-api/sc-activities/${scaID.random()}/supply-chains")
		.check(status.is(session => 200))
		)}

	// val getSupplyChains = exec(http("Get Supply Chains")
	// 	.get("/planning-api/supply-chains")
	// 	.check(jsonPath("$.item_list[*].id")
	// 	.findAll.saveAs("sc-ids"))
	// 	.check(status.is(session => 200))
	// )

	val deleteSupplyChains = {
		exec(http("Delete SCA supply chain")
		.delete("/planning-api/supply-chains/${sc-ids.random()}")
		.check(status.is(session => 204))
		)
	}
}
