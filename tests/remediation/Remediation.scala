package specs.remediation

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._

object Remediation {
    val createSecurityMechanism1 = exec(http("Create security mechanism1")
        .post("/service-manager/cloud-sla/security-mechanisms")
        .header("Content-Type", "text/plain")
        .body(ELFileBody("../user-files/data/mechanism-sva.json"))
        .check(status.is(session => 201))
    )
    val createSecurityMechanism2 = exec(http("Create security mechanism2")
        .post("/service-manager/cloud-sla/security-mechanisms")
        .header("Content-Type", "text/plain")
        .body(ELFileBody("../user-files/data/mechanism-webpool.json"))
        .check(status.is(session => 201))
    )

    val createRemActivity = exec(http("Create RemActivity")
        .post("/rds-api/rem-activities")
        .header("Content-Type", "application/json")
        .body(ELFileBody("../user-files/data/rem-activity-data.json"))
        .check(jsonPath("$.diag_act_id").find(0).saveAs("DIAGNOSIS_ACTIVITY_ID"))
        .check(status.is(session => 201))
    )
}
