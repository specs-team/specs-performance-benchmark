package specs.securityreasoner

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import com.typesafe.config._

class SecurityReasonerSingleEval_Ramp extends Simulation {
val conf = ConfigFactory.load()
		val baseURL= conf.getString("baseURL")
   val baseUser = conf.getString("numUsers")
   val numUsers = baseUser.toInt
    val baseRamp = conf.getString("rampLength")
   val rampLength = baseRamp.toInt



	val httpProtocol = http
		.baseURL(baseURL)
		.inferHtmlResources(BlackList(""".*\.ico""",""".*\.png""", """.*\.js""", """.*\.css"""), WhiteList())

	

    val uri1 = baseURL+"/security-reasoner"

	val users = scenario("Users Security Reasoner").exec(SingleEval.userSingleEval)
	
	setUp(users.inject(rampUsers(numUsers) over(rampLength seconds)).protocols(httpProtocol))

	




	object SingleEval  {

	val headers_0 = Map("Upgrade-Insecure-Requests" -> "1")

	val headers_2 = Map(
		"Accept-Encoding" -> "gzip, deflate",
		"Origin" -> baseURL,
		"Upgrade-Insecure-Requests" -> "1")

	val headers_5 = Map(
		"Accept" -> "*/*",
		"X-Requested-With" -> "XMLHttpRequest")


	// scenario dell'utente
	val userSingleEval = /*exec(http("Welcome")
			.get("/security-reasoner/Welcome.do")
			.headers(headers_0))
		.pause(1)*/
		exec(http("Select Single Evaluation")
			.post("/security-reasoner/user/SelectEvaluationAction.do")
			.headers(headers_2)
			.formParam("selectedCaiqs", "76$Amazon EC2 -CAIQ-v2-2013-11-01")
			.formParam("selectedJudgments", "27$judgment 1 (administrator)")
			.formParam("metodo", "singleEvaluation"))
		

}

}