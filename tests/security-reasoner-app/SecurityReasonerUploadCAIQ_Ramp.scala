package specs.securityreasoner

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import com.typesafe.config._

class SecurityReasonerUploadCAIQ_Ramp extends Simulation {



	val conf = ConfigFactory.load()
	val baseURL= conf.getString("baseURL")
   val baseUser = conf.getString("numUsers")
   val numUsers = baseUser.toInt
   val baseRamp = conf.getString("rampLength")
   val rampLength = baseRamp.toInt
   
   
   
	val httpProtocol = http
		.baseURL(baseURL)
		.inferHtmlResources(BlackList(""".*\.ico""",""".*\.png""", """.*\.js""", """.*\.css"""), WhiteList())

	

    val uri1 = baseURL+"/security-reasoner"
	
	val users = scenario("Users Security Reasoner").exec(UploadCAIQ.uploadcaiq)

	setUp(users.inject(rampUsers(numUsers) over(rampLength seconds)).protocols(httpProtocol))
	




	


object UploadCAIQ  {

	val headers_0 = Map("Upgrade-Insecure-Requests" -> "1")

	val headers_4 = Map(
		"Accept-Encoding" -> "gzip, deflate",
		"Content-Type" -> "multipart/form-data; boundary=----WebKitFormBoundaryOgFEPchFOEa6UhZV",
		"Origin" -> baseURL,
		"Upgrade-Insecure-Requests" -> "1")

	val headers_6 = Map(
		"Accept-Encoding" -> "gzip, deflate",
		"Origin" -> baseURL,
		"Upgrade-Insecure-Requests" -> "1")

    val uri1 = baseURL+"/security-reasoner"

    val uploadcaiq = exec(http("Welcome")
			.get("/security-reasoner/Welcome.do")
			.headers(headers_0))
		.pause(1)
		.exec(http("Upload CAIQ")
			.post("/security-reasoner/admin/UploadCaicFromXmlAction.do")
			.headers(headers_4)
			.body(RawFileBody("ExpertSecurityReasoner_0004_request.txt")))

}
}