package specs.securityreasoner

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import com.typesafe.config._


class SecurityReasonerMultipleEval_Inject extends Simulation {
val conf = ConfigFactory.load()
	val baseURL= conf.getString("baseURL")
   val baseUser = conf.getString("numUsers")
   val numUsers = baseUser.toInt


	val httpProtocol = http
		.baseURL(baseURL)
		.inferHtmlResources(BlackList(""".*\.ico""",""".*\.png""", """.*\.js""", """.*\.css"""), WhiteList())

	

    val uri1 = baseURL+"/security-reasoner"

	val users = scenario("Users Security Reasoner").exec(MultipleEval.userMultipleEval)
	

	setUp(users.inject(atOnceUsers(numUsers)).protocols(httpProtocol))




	object MultipleEval  {

	val headers_0 = Map("Upgrade-Insecure-Requests" -> "1")

	val headers_2 = Map(
		"Accept-Encoding" -> "gzip, deflate",
		"Origin" -> baseURL,
		"Upgrade-Insecure-Requests" -> "1")

	val headers_5 = Map(
		"Accept" -> "*/*",
		"X-Requested-With" -> "XMLHttpRequest")


	// scenario dell'utente
	val userMultipleEval = exec(http("Multiple Evaluation")
			.post("/security-reasoner/user/SelectEvaluationAction.do")
			.headers(headers_2)
			.formParam("selectedCaiqs", "76$Amazon EC2 -CAIQ-v2-2013-11-01")
			.formParam("selectedCaiqs", "77$CloudSigma-CAIQ-v1.2.1-2013-07-30")
			.formParam("selectedCaiqs", "78$Softlayer-CAIQ-v1.1.-2012-11-05")
			.formParam("selectedJudgments", "27$judgment 1 (administrator)")
			.formParam("metodo", "multipleEvaluation"))
		

}
}