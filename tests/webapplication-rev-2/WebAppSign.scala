package webapplication

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class WebAppSign extends Simulation {

	val httpProtocol = http
		.baseURL("http://194.102.62.242:8080")
		.inferHtmlResources(BlackList(""".*\.css""", """.*\.js""", """.*\.ico"""), WhiteList())

	val headers_0 = Map("Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
	
	val headers_1 = Map("Content-type" -> "application/json")

    val uri1 = "http://194.102.62.242:8080/webcontainer-app-rev2"

	val scn = scenario("WebAppSign")
		.exec(http("request_0")
			.get("/webcontainer-app-rev2/")
			.headers(headers_0))
		.pause(2)
		.exec(http("request_1")
			.get("/webcontainer-app-rev2/rest/services/"))
		.pause(1)
		.exec(http("request_2")
			.get("/webcontainer-app-rev2/rest/capabilities/WebPool_SVA")
			.check(jsonPath("$.idsla").find.saveAs("idSla")))
		.pause(2)
		.exec(http("request_3")
			.post("/webcontainer-app-rev2/rest/securities/")
			.headers(headers_1)
			.body(StringBody("""{"capabilities":[{"id":"WEBPOOL"},{"id":"SVA"}],"idsla":"${idSla}"}""")))
		.pause(3)
		.exec(http("request_4")
			.post("/webcontainer-app-rev2/rest/agreements/")
			.headers(headers_1)
			.body(StringBody("""{"capabilities":[{"id":"WEBPOOL","frameworks":[{"id":"NIST_800_53_r4","securityControls":[{"id":"WEBPOOL_NIST_CP_2","weight":"MEDIUM"},{"id":"WEBPOOL_NIST_SC_29","weight":"MEDIUM"},{"id":"WEBPOOL_NIST_SC_36","weight":"MEDIUM"},{"id":"WEBPOOL_NIST_SC_5","weight":"MEDIUM"}]}]},{"id":"SVA","frameworks":[{"id":"NIST_800_53_r4","securityControls":[{"id":"SVA_NIST_CA_7","weight":"MEDIUM"},{"id":"SVA_NIST_CA_7_3","weight":"MEDIUM"},{"id":"SVA_NIST_CA_8","weight":"MEDIUM"},{"id":"SVA_NIST_RA_5","weight":"MEDIUM"},{"id":"SVA_NIST_RA_5_1","weight":"MEDIUM"}]}]}],"idsla":"${idSla}"}""")))
		.pause(4)
		.exec(http("request_5")
			.post("/webcontainer-app-rev2/rest/overview/")
			.headers(headers_1)
			.body(StringBody("""{"capabilities":[{"id":"WEBPOOL","frameworks":[{"id":"NIST_800_53_r4","securityControls":[{"id":"WEBPOOL_NIST_CP_2","weight":"MEDIUM","metrics":[{"id":"level_of_diversity_m2","importance":"MEDIUM","operator":"ge","operand":"1"},{"id":"level_of_redundancy_m1","importance":"MEDIUM","operator":"ge","operand":"1"}]},{"id":"WEBPOOL_NIST_SC_29","weight":"MEDIUM","metrics":[{"id":"level_of_diversity_m2","importance":"MEDIUM","operator":"ge","operand":"1"}]},{"id":"WEBPOOL_NIST_SC_36","weight":"MEDIUM","metrics":[{"id":"level_of_redundancy_m1","importance":"MEDIUM","operator":"ge","operand":"1"}]},{"id":"WEBPOOL_NIST_SC_5","weight":"MEDIUM","metrics":[{"id":"level_of_diversity_m2","importance":"MEDIUM","operator":"ge","operand":"1"},{"id":"level_of_redundancy_m1","importance":"MEDIUM","operator":"ge","operand":"1"}]}]}]},{"id":"SVA","frameworks":[{"id":"NIST_800_53_r4","securityControls":[{"id":"SVA_NIST_CA_7","weight":"MEDIUM","metrics":[{"id":"extended_scan_frequency_m22","importance":"MEDIUM","operator":"eq","operand":"24"},{"id":"basic_scan_frequency_m13","importance":"MEDIUM","operator":"eq","operand":"24"},{"id":"up_report_frequency_m23","importance":"MEDIUM","operator":"eq","operand":"24"}]},{"id":"SVA_NIST_CA_7_3","weight":"MEDIUM","metrics":[{"id":"list_update_frequency_m14","importance":"MEDIUM","operator":"eq","operand":"72"}]},{"id":"SVA_NIST_CA_8","weight":"MEDIUM","metrics":[{"id":"pen_testing_activated_m24","importance":"MEDIUM","operator":"eq","operand":"yes"}]},{"id":"SVA_NIST_RA_5","weight":"MEDIUM","metrics":[{"id":"extended_scan_frequency_m22","importance":"MEDIUM","operator":"eq","operand":"24"},{"id":"basic_scan_frequency_m13","importance":"MEDIUM","operator":"eq","operand":"24"},{"id":"up_report_frequency_m23","importance":"MEDIUM","operator":"eq","operand":"24"}]},{"id":"SVA_NIST_RA_5_1","weight":"MEDIUM","metrics":[{"id":"list_update_frequency_m14","importance":"MEDIUM","operator":"eq","operand":"72"}]}]}]}],"idsla":"${idSla}"}""")))
		.pause(1)
		.exec(http("request_6")
			.post("/webcontainer-app-rev2/rest/submitOffer/")
			.headers(headers_1)
			.body(StringBody(""""{"id":"${idSla}_offer1","xml":"","idsla":"${idSla}"}""""))
			.resources(http("request_7")
			.get(uri1 + "/rest/incomplete/negotiating/")))
		.pause(1)
		.exec(http("request_8")
			.post("/webcontainer-app-rev2/rest/sign/${idSla}")
			.headers(headers_1)
			.resources(http("request_9")
			.get(uri1 + "/rest/incomplete/signed/")))

	setUp(scn.inject(constantUsersPerSec(4) during(2 minutes)).protocols(httpProtocol))
}
