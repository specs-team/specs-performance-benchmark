package specs.slamanager
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import com.typesafe.config._

class deleteSLARamp extends Simulation {

	val conf = ConfigFactory.load()
	val baseURL= conf.getString("baseURL")
   val baseUser = conf.getString("numUsers")
   val numUsers = baseUser.toInt
   val baseRamp = conf.getString("rampLength")
   val rampLength = baseRamp.toInt


	val httpProtocol = http
		.baseURL(baseURL)
		.inferHtmlResources()



    val uri1 = baseURL

	/*val scn = scenario("Get SLAs")
		.exec(http("getList")
			.get("/sla-manager/cloud-sla/slas"))
		.pause(6)
		.exec(http("getSLA")
			.get("/sla-manager/cloud-sla/slas/56A743FBE4B08C6B6B88DA64"))*/
	val users = scenario("Browse SLAs").exec(Delete.browse,Delete.delete)

	setUp(users.inject(rampUsers(numUsers) over(rampLength seconds)).protocols(httpProtocol))


object Delete {



  
  val browse = exec(http("Get SLAs IDs")
			.get("/sla-manager/cloud-sla/slas")
			.check(
			   xpath("//@id")
			.findAll.saveAs("slaid"))
			.check(
				xpath("//@id").count.saveAs("countId")
				)
			)
			//.pause(1)
			/*
    		.exec(http("Select SLA")
    		.get("/sla-manager/cloud-sla/slas/${slaid}")) // 6
    		.pause(1)
    		*/

   val delete=exec(http("Delete SLA")
    		.delete("/sla-manager/cloud-sla/slas/${slaid.random()}")) // 6
    		//.pause(1)
}
}