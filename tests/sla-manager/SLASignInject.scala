package specs.slamanager
import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._


class signSLAInject extends Simulation {

val conf = ConfigFactory.load()
val baseURL= conf.getString("baseURL")
   val baseUser = conf.getString("numUsers")
   val numUsers = baseUser.toInt

  val httpProtocol = http
    .baseURL(baseURL)
    .inferHtmlResources()



    val uri1 = baseURL

  /*val scn = scenario("Get SLAs")
    .exec(http("getList")
      .get("/sla-manager/cloud-sla/slas"))
    .pause(6)
    .exec(http("getSLA")
      .get("/sla-manager/cloud-sla/slas/56A743FBE4B08C6B6B88DA64"))*/
  val users = scenario("Browse SLAs").exec(Browse.browse,Browse.select,Browse.status/*,Browse.sign*/)

   
  setUp(users.inject(atOnceUsers(numUsers)).protocols(httpProtocol))





object Browse {



  
  val browse = exec(http("Get SLAs IDs")
      .get("/sla-manager/cloud-sla/slas")
      .check(
         xpath("//@id")
      .findAll.saveAs("slaid"))
      .check(
        xpath("//@id").count.saveAs("countId")
        )
      )
      //.pause(1)
      /*
        .exec(http("Select SLA")
        .get("/sla-manager/cloud-sla/slas/${slaid}")) // 6
        .pause(1)
        */

   val select=repeat(1){
        exec(http("Select SLA")
        .get("/sla-manager/cloud-sla/slas/${slaid.random()}")) // 6
        //.pause(1)
      }

  

    val status= repeat(1){

    exec(http("Get SLAs IDs")
      .get("/sla-manager/cloud-sla/slas")
      .check(
         xpath("//@id")
      .findAll.saveAs("slaid"))
      .check(
        xpath("//@id").count.saveAs("countId")
        )
      )

    var slaID = ""
    val random = new java.util.Random
    exec { session =>
  val list =  session("slaid").as[Seq[String]]
  session.set("randomSM", list(random.nextInt(list.size)))
  //slaID = session("slaid").as[String] 
   //session.set("randomSM", slaID)
  }
    
    

      .exec(http("Get SLA State")
        .get("/sla-manager/cloud-sla/slas/${randomSM}/status")
        .check(bodyString.saveAs("neg"))
      )
      .doIf("${neg}","NEGOTIATING"){
      exec(http("Sign SLA")
      .post("/sla-manager/cloud-sla/slas/${randomSM}/sign")
      .body(RawFileBody("../user-files/data/SLADosReportMaxAge.xml")))
     }
     /*
     .exec(http("Get SLA State")
        .get("/sla-manager/cloud-sla/slas/${randomSM}/status")
        .check(bodyString.saveAs("sig"))
      )
      .doIf("${sig}","SIGNED"){
      exec(http("Observe SLA")
      .post("/sla-manager/cloud-sla/slas/${randomSM}/observe"))
     }
      .exec(http("Get SLA State")
        .get("/sla-manager/cloud-sla/slas/${randomSM}/status")
        .check(bodyString.saveAs("obs"))
      )
      .doIf("${obs}","OBSERVED"){
      exec(http("Complete SLA")
      .post("/sla-manager/cloud-sla/slas/${randomSM}/complete"))
    }*/
    }
     

  
}
}