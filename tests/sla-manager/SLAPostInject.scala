package specs.slamanager

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import com.typesafe.config._

class postSLAsInject extends Simulation {

	val conf = ConfigFactory.load()
	val baseURL= conf.getString("baseURL")
   val baseUser = conf.getString("numUsers")
   val numUsers = baseUser.toInt


	val httpProtocol = http
		.baseURL(baseURL)
		.inferHtmlResources()

    val uri1 = baseURL

    // passare tutto il path quando si aggiunge un file
	val users = scenario("Post a SLA")
		.exec(http("POST a SLA")
			.post("/sla-manager/cloud-sla/slas")
			.body(RawFileBody("../user-files/data/SLADosReportMaxAge.xml")))
			
	setUp(users.inject(atOnceUsers(numUsers)).protocols(httpProtocol))
	
	

}