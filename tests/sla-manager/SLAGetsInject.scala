package specs.slamanager
import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._


class getSLAsListandSelectInject extends Simulation {
	val conf = ConfigFactory.load()
	val baseURL= conf.getString("baseURL")
   val baseUser = conf.getString("numUsers")
   val numUsers = baseUser.toInt



	
  val httpProtocol = http
    .baseURL(baseURL)
    .inferHtmlResources()



    val uri1 = baseURL

  /*val scn = scenario("Get SLAs")
    .exec(http("getList")
      .get("/sla-manager/cloud-sla/slas"))
    .pause(6)
    .exec(http("getSLA")
      .get("/sla-manager/cloud-sla/slas/56A743FBE4B08C6B6B88DA64"))*/
  val users = scenario("Browse SLAs").exec(Browse.browse100,Browse.select/*,Browse.sign*/)



	setUp(users.inject(atOnceUsers(numUsers)).protocols(httpProtocol))



object Browse {



  
  val browse = exec(http("Get SLAs IDs")
      .get("/sla-manager/cloud-sla/slas")
      .check(
         xpath("//@id")
      .findAll.saveAs("slaid"))
      .check(
        xpath("//@id").count.saveAs("countId")
        )
      )
      //.pause(1)
      /*
        .exec(http("Select SLA")
        .get("/sla-manager/cloud-sla/slas/${slaid}")) // 6
        .pause(1)
        */

   val select=repeat(1){
        exec(http("Select SLA")
        .get("/sla-manager/cloud-sla/slas/${slaid.random()}")) // 6
        //.pause(1)
      }


    val browse100 = exec(http("Get SLAs IDs")
      .get("/sla-manager/cloud-sla/slas?items=100")
      .check(
         xpath("//@id")
      .findAll.saveAs("slaid"))
      .check(
        xpath("//@id").count.saveAs("countId")
        )
      )

}
}