package specs.webapp

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import com.typesafe.config._


// mi  fermo al sign, voglio capire quante negoziazioni riesco a fare contemporaneamente e qual'è il limite di utenti

class SPECSWebAppSignRamp extends Simulation {

	val conf = ConfigFactory.load()
	val baseURL= conf.getString("baseURL")
	val baseUser = conf.getString("numUsers")
	val numUsers = baseUser.toInt
	val baseRamp = conf.getString("rampLength")
   val rampLength = baseRamp.toInt


	val httpProtocol = http
		.baseURL(baseURL)
		.inferHtmlResources(BlackList(""".*\.js""", """.*\.css""", """.*\.ico""",""".*\.png"""), WhiteList())

	val headers_0 = Map(
		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, sdch",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_2 = Map("Accept-Encoding" -> "gzip, deflate, sdch")

	val headers_3 = Map(
		"Content-type" -> "application/json",
		"Origin" -> baseURL)

	val headers_8 = Map(
		"Content-type" -> "text/plain",
		"Origin" -> baseURL)

    val uri1 = baseURL

	val scn = scenario("SPECSWebApp2")
		.exec(http("Home")
			.get("/specs-app-webcontainer-demo/")
			.headers(headers_0))
		.pause(3)
		.exec(http("Negotiation")
			.get("/specs-app-webcontainer-demo/negotiation.jsp")
			.headers(headers_0))
		.pause(1)
		.exec(http("SLA Template")
			.get("/specs-app-webcontainer-demo/services/rest/slaTemplate")
			.headers(headers_2)
			.check(jsonPath("$.id").find.saveAs("idSla")))
		.pause(3)
		.exec(http("Submit SLA")
			.post("/specs-app-webcontainer-demo/services/rest/submit")
			.headers(headers_3)
			.body(StringBody("""{"id":"${idSla}","xml":"<?xml version=\"1.0\" encoding=\"UTF-8\"?><wsag:AgreementOffer xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:wsag=\"http://schemas.ggf.org/graap/2007/03/ws-agreement\" xmlns:specs=\"http://specs-project.eu/schemas/sla_related\" xmlns:ccm=\"http://specs-project.eu/schemas/ccm_control_framework\" xmlns:nist=\"http://specs-project.eu/schemas/nist_control_framework\" xsi:schemaLocation=\"http://schemas.ggf.org/graap/2007/03/ws-agreement http://schemas.ggf.org/graap/2007/03/ws-agreement        http://www.w3.org/2001/XMLSchema http://www.w3.org/2001/XMLSchema       http://specs-project.eu/schemas/sla_related specs.xsd       http://specs-project.eu/schemas/ccm_control_framework ccm_control_framework.xsd       http://specs-project.eu/schemas/nist_control_framework        nist_control_framework.xsd\">        <wsag:Name>Y2-SPECS-APP</wsag:Name>    <wsag:Context>  <wsag:AgreementInitiator>$SPECS-CUSTOMER</wsag:AgreementInitiator>   <wsag:AgreementResponder>$SPECS-APPLICATION</wsag:AgreementResponder>    <wsag:ServiceProvider>AgreementResponder</wsag:ServiceProvider>    <wsag:ExpirationTime>2014-02-02T06:00:00</wsag:ExpirationTime>     <wsag:TemplateName>Y2-APP-TEMPLATE</wsag:TemplateName>    </wsag:Context>        <wsag:Terms>  \t<wsag:All>  \t\t   \t\t <wsag:ServiceDescriptionTerm wsag:Name=\"Secure Web Server\" wsag:ServiceName=\"SecureWebServer\">\t   \t\t\t   \t\t\t <specs:serviceDescription>  \t\t\t   \t\t\t\t\t   \t\t\t \t<specs:service_resources>   \t\t\t\t\t <specs:resources_provider id=\"aws-ec2\" name=\"Amazon\" zone=\"us-east-1\">  \t\t\t\t\t\t<specs:VM appliance=\"us-east-1/ami-ff0e0696\" hardware=\"t1.micro\" descr=\"open suse 13.1 on amazon EC2\"/>  \t\t\t\t\t </specs:resources_provider>  \t\t\t \t</specs:service_resources>  \t\t  \t\t\t  \t\t\t<specs:capabilities>    \t\t\t\t    \t\t\t\t\t      \t\t\t\t    \t\t\t\t<specs:capability id=\"OPENVAS\" name=\"Vulnerability Scanning\" description=\"Capability of detecting the vulnerabilities a machine (along with the installed software) is subject to.\">\t\t \t   \t\t\t             <specs:control_framework id=\"CCM_v3.0\" frameworkName=\"CCM Control framework v3.0\">  \t\t\t            \t<specs:security_control id=\"OPENVAS_CCM_TMV_02\" name=\"Vulnerability / Patch Management - TVM-02\" ccm:control_family=\"TMV\" ccm:security_control=\"02\" ccm:control_enhancement=\"\">MEDIUM</specs:security_control>  \t\t\t\t\t\t\t\t\t  \t\t\t\t\t\t </specs:control_framework>  \t\t\t\t\t\t   \t\t\t\t</specs:capability>  \t\t\t\t    \t\t\t\t<specs:capability id=\"OSSEC\" name=\"DOS Detection and Mitigation\" description=\"Capability of detecting and reacting to security attacks aimed at distrupting a system's availability.\">\t\t   \t\t\t\t\t  \t   \t\t\t\t\t\t   \t\t\t             <specs:control_framework id=\"CCM_v3.0\" frameworkName=\"CCM Control framework v3.0\">  \t\t\t            \t\t\t\t\t\t\t\t  \t\t\t            \t  <specs:security_control id=\"OSSEC_CCM_IVS_13\" name=\"DENIAL OF SERVICE PROTECTION - IVS-13\" ccm:control_family=\"IVS\" ccm:security_control=\"13\" ccm:control_enhancement=\"\">MEDIUM</specs:security_control>\t\t\t\t\t\t   \t\t\t\t\t\t   \t\t\t\t\t\t </specs:control_framework>  \t\t\t\t\t\t   \t\t\t\t</specs:capability>  \t\t\t\t  \t  \t</specs:capabilities>  \t\t\t \t   \t\t\t \t   \t\t\t </specs:serviceDescription>  \t\t </wsag:ServiceDescriptionTerm>  \t\t      \t      \t      \t<wsag:ServiceReference wsag:Name=\"SecureWebServer_endpoint\" wsag:ServiceName=\"SecureWebServer\">  \t \t<specs:endpoint>http://specs/application-endpoint</specs:endpoint>  \t</wsag:ServiceReference>              \t<wsag:ServiceProperties wsag:Name=\"//specs:capability[@id='WEBPOOL']\" wsag:ServiceName=\"SecureWebServer\">  \t\t<wsag:VariableSet>  \t\t\t  \t\t\t<wsag:Variable wsag:Name=\"specs_webpool_M1\" wsag:Metric=\"specs.eu/metrics/M1_redundancy\">  \t\t\t\t<wsag:Location>//specs:security_control[@id='WEBPOOL_CCM_BCR_01']</wsag:Location>  \t\t\t</wsag:Variable>  \t\t\t  \t\t\t<wsag:Variable wsag:Name=\"specs_webpool_M2\" wsag:Metric=\"specs.eu/metrics/M2_diversity\">  \t\t\t\t<wsag:Location>//specs:security_control[@id='WEBPOOL_CCM_BCR_01']</wsag:Location>  \t\t\t</wsag:Variable>  \t\t  \t\t</wsag:VariableSet>\t  \t</wsag:ServiceProperties>            \t<wsag:ServiceProperties wsag:Name=\"//specs:capability[@id='OPENVAS']\" wsag:ServiceName=\"SecureWebServer\">  \t\t<wsag:VariableSet>  \t\t\t   \t\t<wsag:Variable wsag:Name=\"specs_openvas_M13\" wsag:Metric=\"specs.eu/metrics/M13_vulnerability_report_max_age\">  \t\t\t\t<wsag:Location>//specs:security_control[@id='OPENVAS_CCM_TMV_02']</wsag:Location>  \t   </wsag:Variable>  \t\t  \t\t<wsag:Variable wsag:Name=\"specs_openvas_M14\" wsag:Metric=\"specs.eu/metrics/M14_vulnerability_list_max_age\">  \t\t\t\t<wsag:Location>//specs:security_control[@id='OPENVAS_CCM_TMV_02']</wsag:Location>  \t   </wsag:Variable>  \t\t\t   \t\t\t   \t\t</wsag:VariableSet>\t  \t</wsag:ServiceProperties>      \t<wsag:ServiceProperties wsag:Name=\"//specs:capability[@id='OSSEC']\" wsag:ServiceName=\"SecureWebServer\">  \t\t<wsag:VariableSet>  \t\t\t   \t\t\t<wsag:Variable wsag:Name=\"specs_ossec_M16\" wsag:Metric=\"specs.eu/metrics/M16_dos_report_max_age\">  \t\t\t\t<wsag:Location>//specs:security_control[@id='OSSEC_CCM_IVS_13']</wsag:Location>  \t      </wsag:Variable>  \t\t    \t\t\t   \t\t\t   \t\t</wsag:VariableSet>\t  \t</wsag:ServiceProperties>                      <wsag:GuaranteeTerm wsag:Name=\"//specs:capability[@id='OPENVAS']\" wsag:Obligated=\"ServiceProvider\">     <wsag:ServiceScope wsag:ServiceName=\"SecureWebServer\">    </wsag:ServiceScope>    \t<wsag:QualifyingCondition>   \tTRUE  \t</wsag:QualifyingCondition>       <wsag:ServiceLevelObjective>  \t \t<wsag:CustomServiceLevel>  \t \t\t<specs:objectiveList>  \t \t\t\t  \t \t\t\t  \t \t\t\t<specs:SLO name=\"specs_openvas_M13\" metric-name=\"Vulnerability Report Max Age\">  \t \t\t\t\t<specs:description>The frequency of report generation (e.g., a value of \"7*24h\" requires that reports are generated at least once per week).</specs:description>  \t\t\t\t\t<specs:expression unit=\"hours\" op=\"eq\">24</specs:expression>  \t\t\t\t\t<specs:importance_weight>MEDIUM</specs:importance_weight>  \t\t\t\t</specs:SLO>  \t\t\t    <specs:SLO name=\"specs_openvas_M14\" metric-name=\"Vulnerability List Max Age\">  \t \t\t\t\t<specs:description>The frequency of vulnerability list updates (e.g., a value of \"24h\" requires that the list of known vulnerabilities is updated at least once per day).</specs:description>  \t\t\t\t\t<specs:expression unit=\"hours\" op=\"eq\">72</specs:expression>  \t\t\t\t\t<specs:importance_weight>MEDIUM</specs:importance_weight>  \t\t\t\t</specs:SLO>  \t \t\t  \t \t\t  \t \t\t  \t \t\t</specs:objectiveList>  \t \t</wsag:CustomServiceLevel>     </wsag:ServiceLevelObjective>    \t  \t<wsag:BusinessValueList>  \t  \t</wsag:BusinessValueList>    </wsag:GuaranteeTerm>        <wsag:GuaranteeTerm wsag:Name=\"//specs:capability[@id='OSSEC']\" wsag:Obligated=\"ServiceProvider\">     <wsag:ServiceScope wsag:ServiceName=\"SecureWebServer\">    </wsag:ServiceScope>    \t<wsag:QualifyingCondition>   \tTRUE  \t</wsag:QualifyingCondition>       <wsag:ServiceLevelObjective>  \t \t<wsag:CustomServiceLevel>  \t \t\t<specs:objectiveList>    \t\t\t    <specs:SLO name=\"specs_ossec_M16\" metric-name=\"DOS Attack Report Max Age\">  \t \t\t\t\t<specs:description>The frequency of attack report generation (e.g., a value of \"24h\" requires that reports are generated every two hours)</specs:description>  \t\t\t\t\t<specs:expression unit=\"hours\" op=\"eq\">24</specs:expression>  \t\t\t\t\t<specs:importance_weight>MEDIUM</specs:importance_weight>  \t\t\t\t</specs:SLO>  \t \t\t\t\t \t\t  \t \t\t</specs:objectiveList>  \t \t</wsag:CustomServiceLevel>     </wsag:ServiceLevelObjective>    \t  \t<wsag:BusinessValueList>  \t  \t</wsag:BusinessValueList>    </wsag:GuaranteeTerm>      \t  </wsag:All>  </wsag:Terms>        </wsag:AgreementOffer>"}"""))
			.resources(http("request_4")
			.get(uri1 + "/sign.do")
			.headers(headers_0)))
		.pause(3)
		.exec(http("Sign SLA")
			.post("/specs-app-webcontainer-demo/services/rest/sign")
			.headers(headers_3)
			.body(StringBody("""{"id":${idSla},"xml":null}"""))
			.resources(http("request_6")
			.get(uri1 + "/implement.do")
			.headers(headers_0)))

	setUp(scn.inject(rampUsers(numUsers) over(rampLength seconds)).protocols(httpProtocol))
}