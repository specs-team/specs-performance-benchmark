package specs.securityreasoner
import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._
import java.util.concurrent.ThreadLocalRandom


class evaluateCaiqsInject extends Simulation {

  val baseURL= "http://194.102.62.208:8080"


  val httpProtocol = http
    .baseURL(baseURL)
    .inferHtmlResources()



    val uri1 = baseURL
	  
  val users = scenario("Evaluate CAIQs").exec(Browse.associateCaiq)


 setUp(users.inject(constantUsersPerSec(30) during(2 minutes)).protocols(httpProtocol))

  


object Browse {

    val associateCaiq = exec(
        http("Get Caiq")
        .get("/security-reasoner/caiqs")
        .check(jsonPath("""$.item[*].id""").findAll.saveAs("listCaiqs")))
		.exec(http("Get Judgement")
        .get("/security-reasoner/judgements")
        .check(jsonPath("""$.item[*].id""").findAll.saveAs("listJudgements")))
    .exec(http("Associate Caiq")
        .post("/security-reasoner/caiqs/571609C5E4B098262A2AC9A5/associate")
        .body(StringBody("""57161FDCE4B098262A2AC9FE""")))
    .exec(http("Evaluate Caiq")
        .get("/security-reasoner/caiqs/571609C5E4B098262A2AC9A5/evaluate"))
    /*
    val caiqId = "${listCaiqs.random()}"

    val evaluateCaiq = exec(http("Associate Caiq")
        .post("/security-reasoner/caiqs/${caiqId}/associate")
		    .body(StringBody("${listJudgements.random()}")))
    .exec(http("Evaluate Caiq")
        .get("/security-reasoner/caiqs/${caiqId}/evaluate"))
        */

}



  
 
}
