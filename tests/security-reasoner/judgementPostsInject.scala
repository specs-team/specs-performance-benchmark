package specs.securityreasoner

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import java.util.concurrent.ThreadLocalRandom
import com.typesafe.config._


class uploadJudgementInject extends Simulation {


	val baseURL= "http://194.102.62.242:8080"
	
	val httpProtocol = http
		.baseURL(baseURL)
		.inferHtmlResources()



    val uri1 = baseURL
	
	val users = scenario("Upload Judgement").exec(Upload.uploadjudgement)

	
	setUp(users.inject(constantUsersPerSec(5) during(2 seconds)).protocols(httpProtocol))

object Upload{

	val uploadjudgement = scenario("Upload Judgement")
		.exec(http("Upload Judgement")
			.post("/security-reasoner/judgements")
			.body(RawFileBody("../user-files/data/judgement1.xml")))

	
		
}

}