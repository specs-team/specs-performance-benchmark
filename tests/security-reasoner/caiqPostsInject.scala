package specs.securityreasoner

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import java.util.concurrent.ThreadLocalRandom
import com.typesafe.config._


class uploadCaiqInject extends Simulation {


	val baseURL= "http://194.102.62.208:8080"
	
	val headers_1 = Map("Content-type" -> "multipart/form-data")

	val httpProtocol = http
		.baseURL(baseURL)
		.inferHtmlResources()



    val uri1 = baseURL
	
	val users = scenario("Upload Caiq").exec(Upload.uploadcaiq)

	
	setUp(users.inject(constantUsersPerSec(5) during(120 seconds)).protocols(httpProtocol))

object Upload{

	val uploadcaiq = scenario("Upload Caiq")
		.exec(http("Upload Caiq")
			.post("/security-reasoner/caiqs")
			.formUpload("file", "../user-files/data/Amazon-EC2-CAIQ-CCM-3.0.xml"))

	
		
}

}