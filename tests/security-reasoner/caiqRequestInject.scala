package specs.securityreasoner
import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._


class getCaiqsInject extends Simulation {

  val baseURL= "http://194.102.62.208:8080"


  val httpProtocol = http
    .baseURL(baseURL)
    .inferHtmlResources()



    val uri1 = baseURL
	  
  val users = scenario("Get CAIQs").exec(Browse.getAllCaiqs)


 setUp(users.inject(constantUsersPerSec(70) during(2 minutes)).protocols(httpProtocol))

  


object Browse {


  val getAllCaiqs = exec(http("Get ALL Caiqs")
        .get("/security-reasoner/caiqs"))


}


  
 
}
