package specs.securityreasoner
import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._


class getJudgementsInject extends Simulation {

  val baseURL= "http://194.102.62.240:8080"


  val httpProtocol = http
    .baseURL(baseURL)
    .inferHtmlResources()



    val uri1 = baseURL
    
  val users = scenario("Get Judgements").exec(Browse.getJudgement)


 setUp(users.inject(constantUsersPerSec(30) during(2 minutes)).protocols(httpProtocol))

  


object Browse {


  //val getAllJudgements = exec(http("Get ALL Judgements")
    //    .get("/security-reasoner/judgements"))

  val getJudgement = exec(http("Get ALL Judgements")
        .get("/security-reasoner/judgements")
        .check(jsonPath("""$.item[*].id""").findAll.saveAs("listJudgements")))
      .exec(http("Get Judgement")
        .get("/security-reasoner/judgements/${listJudgements.random()}"))

}


  
 
}
