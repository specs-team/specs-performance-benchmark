package specs.servicemanager
import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._


class getServicesInject extends Simulation {

val conf = ConfigFactory.load()
	val baseURL= conf.getString("baseURL")
   val baseUser = conf.getString("numUsers")
   val numUsers = baseUser.toInt


  val httpProtocol = http
    .baseURL(baseURL)
    .inferHtmlResources()



    val uri1 = baseURL

  /*val scn = scenario("Get SLAs")
    .exec(http("getList")
      .get("/sla-manager/cloud-sla/slas"))
    .pause(6)
    .exec(http("getSLA")
      .get("/sla-manager/cloud-sla/slas/56A743FBE4B08C6B6B88DA64"))*/
  val users = scenario("Get Security Mechanisms").exec(Browse.getSM/*,Browse.getAllSC*/)

 setUp(users.inject(atOnceUsers(numUsers)).protocols(httpProtocol))


object Browse {

  val getAllSM = exec(http("Get ALL Security Mechanism")
        .get("/service-manager/cloud-sla/security-mechanisms/"))


  val getSM = exec(http("Get ALL Security Mechanism")
      .get("/service-manager/cloud-sla/security-mechanisms/")
      .check(regex("""security-mechanisms\/(.{2,10}?)"[,]{0,1}""").findAll.saveAs("listSMs"))
      )
    .exec(http("Get Security Mechanism")
      .get("/service-manager/cloud-sla/security-mechanisms/${listSMs.random()}"))
    
    


    

   val getAllSC = 
    exec(http("Get All Security Capabilities")
      .get("/service-manager/cloud-sla/security-capabilities/"))  


    /*
   val getSC = 
    exec(http("Get Security Capability")
    .get("/service-manager/cloud-sla/security-capabilities/TODO(ID)"))
*/



}


  
 
}