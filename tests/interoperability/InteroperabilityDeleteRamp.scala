package specs.interoperability

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import java.util.concurrent.ThreadLocalRandom
import com.typesafe.config._


class InteroperabilityDeleteRamp extends Simulation {

val conf = ConfigFactory.load()
	val baseURL= conf.getString("baseURL")
   val baseUser = conf.getString("numUsers")
   val numUsers = baseUser.toInt
   val baseRamp = conf.getString("rampLength")
   val rampLength = baseRamp.toInt

	val httpProtocol = http
		.baseURL(baseURL)
		.inferHtmlResources()



    val uri1 = baseURL

	/*val scn = scenario("Get SLAs")
		.exec(http("getList")
			.get("/sla-manager/cloud-sla/slas"))
		.pause(6)
		.exec(http("getSLA")
			.get("/sla-manager/cloud-sla/slas/56A743FBE4B08C6B6B88DA64"))*/
	
	val users = scenario("Interoperability").exec(Actions.deleteInterface)

	 setUp(users.inject(rampUsers(numUsers) over(rampLength seconds)).protocols(httpProtocol))




object Actions{

	val headers_3 = Map(
		"Content-type" -> "application/json")


    		
	val deleteInterface = exec(http("Get ALL Interfaces")
    		.get("/interoperability-api/interoperability/interfaces")
    		.check(jsonPath("""$.itemList[*].id""").findAll.saveAs("interfaceIds")))
			.pause(4)
			.exec(http("Delete Interface")
    		.delete("/interoperability-api/interoperability/interfaces/${interfaceIds.random()}"))






object RandomGen{
	def randomInt() : Int = {
      return ThreadLocalRandom.current.nextInt(10000)
   }
}

}

}