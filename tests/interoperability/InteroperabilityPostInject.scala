package specs.interoperability

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import java.util.concurrent.ThreadLocalRandom
import com.typesafe.config._


class InteroperabilityPostInject extends Simulation {

val conf = ConfigFactory.load()
	val baseURL= conf.getString("baseURL")
   val baseUser = conf.getString("numUsers")
   val numUsers = baseUser.toInt

	val httpProtocol = http
		.baseURL(baseURL)
		.inferHtmlResources()



    val uri1 = baseURL

	/*val scn = scenario("Get SLAs")
		.exec(http("getList")
			.get("/sla-manager/cloud-sla/slas"))
		.pause(6)
		.exec(http("getSLA")
			.get("/sla-manager/cloud-sla/slas/56A743FBE4B08C6B6B88DA64"))*/
	
	val users = scenario("Interoperability").exec(Actions.create)

	 setUp(users.inject(atOnceUsers(numUsers)).protocols(httpProtocol))



object Actions{

	val headers_3 = Map(
		"Content-type" -> "application/json")

	val create = exec { session =>
 
 	 session.set("random", RandomGen.randomInt())
	}
	.exec(http("Upload Interoperability Interface")
			.post("/interoperability-api/interoperability/interfaces" )
			.headers(headers_3)
			.body(StringBody("""{
 								"id": "ID${random}",
  								"name": "Name",
  								"base_url": "Base URL",
  								"destination_address": "DestinationAddress",
 								"destination_port": "DestinationPort",
  								"destination_uri": "DestinationURI"
																	}"""))
			)

}



object RandomGen{
	def randomInt() : Int = {
      return ThreadLocalRandom.current.nextInt(1000000)
   }
}

}