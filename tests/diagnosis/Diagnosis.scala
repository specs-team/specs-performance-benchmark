package specs.diagnosis

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._

object Diagnosis {
    val createNotification = exec(http("Create notification")
        .post("/diagnosis-api/notifications")
        .header("Content-Type", "application/json")
        .body(ELFileBody("../user-files/data/notification.json"))
        .check(jsonPath("$.diag_act_id").find(0).saveAs("DIAGNOSIS_ACTIVITY_ID"))
        .check(status.is(session => 201))
    ).pause(1)

    val getDiagnosisActivities = exec(http("Get diagnosis activity id")
        .get("/diagnosis-api/diag-activities/${DIAGNOSIS_ACTIVITY_ID}")
        .check(status.is(session => 200))
    )

    val getAllDiagnosisActivities = exec(http("Get all diagnosis activities")
        .get("/diagnosis-api/notifications")
        .check(status.is(sesiion => 200))
    )
}
