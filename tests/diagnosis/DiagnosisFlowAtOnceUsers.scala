package specs.diagnosis

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._
import specs.planning._
import specs.implementation._
import specs.diagnosis._

class DiagnosisFlowAtOnceUsers extends Simulation {
	val conf = ConfigFactory.load()
	val baseURL = conf.getString("baseURL")
	val baseUser = conf.getString("numUsers")
	val numUsers = baseUser.toInt
	val httpProtocol = http.baseURL(baseURL).inferHtmlResources()

	val diagnosis = scenario("Diagnosis Users at Once").exec(
		Planning.createSLA, Planning.createSCA, Planning.getSupplyChains, Implementation.createPlan, Diagnosis.createNotification,
		Diagnosis.getDiagnosisActivities, Diagnosis.getAllDiagnosisActivities
	)
	setUp(diagnosis.inject(atOnceUsers(numUsers)).protocols(httpProtocol))
}
