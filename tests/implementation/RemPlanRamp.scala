package specs.implementation

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._
import specs.planning._
import specs.implementation._

class RemPlanRamp extends Simulation {
    val conf = ConfigFactory.load()
	val baseURL = conf.getString("baseURL")
	val baseUser = conf.getString("numUsers")
	val numUsers = baseUser.toInt
	val baseRamp = conf.getString("rampLength")
    val rampLength = baseRamp.toInt
	val httpProtocol = http.baseURL(baseURL).inferHtmlResources()

	val implementation = scenario("Implementation users ata once").exec(
		Planning.createSLA, Planning.createSCA, Planning.getSupplyChains, Implementation.createPlan, Implementation.createRemPlan, Implementation.getRemPlan
	)
	setUp(implementation.inject(rampUsers(numUsers) over(rampLength seconds)).protocols(httpProtocol))
}
