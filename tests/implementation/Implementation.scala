package specs.implementation

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._

object Implementation {

    val createPlan = exec(http("CreatePlan")
        .post("/implementation-api/impl-plans")
        .header("Content-Type", "application/json")
        .body(ELFileBody("../user-files/data/impl-plan.json"))
        .check(status.is(session => 201))
    )
    val getImplementationPlan = exec(http("GetImplementationPlan")
        .get("/implementation-api/impl-plans/bf8893d8-ecdc-43d9-822f-a4535139d79b")
        .check(status.is(session => 200))
    )

    val createRemPlan = exec(http("CreateRemPlan")
        .post("/implementation-api/rem-plans")
        .header("Content-Type", "application/json")
        .body(ELFileBody("../user-files/data/rem-plan.json"))
        .check(jsonPath("$.rem_plan_id")
		.find(0).saveAs("REM_PLAN_ID"))
        .check(status.is(session => 201))
    )

    val getRemPlan = exec(http("GetRemPlan")
        .get("/implementation-api/rem-plans/${REM_PLAN_ID}")
        .check(status.is(session => 200))
    )
}
