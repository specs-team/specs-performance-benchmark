package specs.slomanager

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._

object SLOManager {

 val customSLA = scala.io.Source.fromFile("./user-files/data/SLADosReportMaxAge.xml").mkString

 val getSLAs = exec(http("Retrieve SLATemplates")
    .get("/sla-manager/cloud-sla/slas")
    .header("Content-Type", "text/xml")
    .check(status.is(200))
    .check(xpath("//item/text()").findAll.saveAs("slas"))
  )

  val deleteSLA = exec(http("Deleting SLA")
    .delete("${slas.random()}")
    .check(status.is(session => 200))
  )

  def randomId() = Random.nextInt(Integer.MAX_VALUE)


  val createSLATemplate =   exec(http("Create SLATemplate")
    .post("/slo-manager-api/sla-negotiation/sla-templates")
    .header("Content-Type", "text/xml")
    .body(StringBody(session => customSLA.replace("<wsag:Name>SPECS_TEMPLATE_NIST_DAMJAN_569BE34703642E1D8C4AA629_offer1</wsag:Name>", "<wsag:Name>" + randomId() + 
"</wsag:Name>")))
    .check(status.is(201))
  )

  val retrieveTemplates = exec(http("Retrieve SLATemplates")
    .get("/slo-manager-api/sla-negotiation/sla-templates")
    .header("Content-Type", "text/xml")
    .check(status.is(200))
    .check(xpath("//itemList/text()").findAll.saveAs("slaTemplates"))
  )

  val retrieveRandomTemplate = exec(http("Retrieving one offer for WebPool-SVA")
    .get("/slo-manager-api/sla-negotiation/sla-templates/SPECS_TEMPLATE_NIST_DAMJAN_569BE34703642E1D8C4AA629_offer1")
    .header("Content-Type", "text/xml")
    .check(status.is(session => 200), bodyString.saveAs("slaTemplate"))
  )

  val startRandomNegotiation = exec(http("Starting negotiation for random")
    .post("/slo-manager-api/sla-negotiation/sla-templates/1825432283")
    .header("Content-Type", "text/xml")
    .check(status.is(session => 200), bodyString.saveAs("negociatedTemplate"))
    .check(xpath("//wsag:Name/text()", List("wsag" -> "http://schemas.ggf.org/graap/2007/03/ws-agreement")).find(0).saveAs("templateName"))
  )

  val startNegotiation = exec(http("Starting negotiation for WebPool-SVA")
    .post("/slo-manager-api/sla-negotiation/sla-templates/WebPool")
    .header("Content-Type", "text/xml")
    .check(status.is(session => 200), bodyString.saveAs("negociatedTemplate"))
    .check(xpath("//wsag:Name/text()", List("wsag" -> "http://schemas.ggf.org/graap/2007/03/ws-agreement")).find(0).saveAs("templateName"))
  )

  val createOffers = exec(http("Creating offers for ${templateName}")
    .post("/slo-manager-api/sla-negotiation/sla-templates/${templateName}/slaoffers")
    .header("Content-Type", "text/xml")
    .body(StringBody("${negociatedTemplate}"))
    .check(status.is(session => 201))
    .check(xpath("//itemList/text()").findAll.saveAs("slaOffers"))
  )

  val retrieveOffer = exec(http("Retrieving one offer for WebPool-SVA")
    .get("${slaOffers.random()}")
    .header("Content-Type", "text/xml")
    .check(status.is(session => 200), bodyString.saveAs("slaOffer"))
  )


  val selectOffer = exec(http("Selecting offer for WebPool-SVA")
    .put("/slo-manager-api/sla-negotiation/sla-templates/${templateName}/slaoffers/current")
    .header("Content-Type", "text/xml")
    .body(StringBody("${slaOffer}"))
    .check(status.is(session => 201))
  )

  val getSelectedOffer = exec(http("Retrieving selected offer for WebPool-SVA")
    .get("/slo-manager-api/sla-negotiation/sla-templates/${templateName}/slaoffers/current")
    .header("Content-Type", "text/xml")
    .check(status.is(session => 200))
  )

  val deleteSLATemplate  = exec(http("Deleting WebPool-SVA SLATemplate")
    .delete("${slaTemplates.random()}")
    .check(status.in(List(204, 404)))
  )

  val deleteWebPoolSVASLATemplate  = exec(http("Deleting WebPool-SVA SLATemplate")
    .delete("/slo-manager-api/sla-negotiation/sla-templates/WebPool_SVA")
    .header("Content-Type", "text/xml")
    .check(status.is(session => 204))
  )

}
