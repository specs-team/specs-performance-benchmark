package specs.slomanager

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._

class SLATemplatePostAtOnce extends Simulation {
  val conf = ConfigFactory.load()
  //	val baseURL = conf.getString("baseURL")
  val baseURL = "http://194.102.62.242:8080"
  val baseUser = "2500"
  val numUsers = baseUser.toInt
  val httpProtocol = http.baseURL(baseURL).inferHtmlResources()

  val negotiation = scenario("SLATemplate create for " + baseUser + " users at once").exec(
    SLOManager.createSLATemplate
  )
  setUp(negotiation.inject(atOnceUsers(numUsers)).protocols(httpProtocol))
}
