package specs.slomanager

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._

class StartNegotiationAtOnce extends Simulation {
  val conf = ConfigFactory.load()
  //	val baseURL = conf.getString("baseURL")
  val baseURL = "http://194.102.62.242:8080"
  val baseUser = "900"
  val numUsers = baseUser.toInt
  val httpProtocol = http.baseURL(baseURL).inferHtmlResources()

  val negotiation = scenario("Starting negotiation for " + baseUser + " users at once").exec(
    SLOManager.startRandomNegotiation
  )
  setUp(negotiation.inject(constantUsersPerSec(numUsers) during(2 seconds)).protocols(httpProtocol))
}
