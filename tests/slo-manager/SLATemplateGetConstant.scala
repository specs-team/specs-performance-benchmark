package specs.slomanager

import scala.concurrent.duration._
import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.validation._
import com.typesafe.config._

class SLATemplateGetRamp extends Simulation {
  val conf = ConfigFactory.load()
  //	val baseURL = conf.getString("baseURL")
  val baseURL = "http://194.102.62.242:8080"
  val baseUser = "1000"
  val numUsers = baseUser.toInt
  val baseRamp = "5"
  val rampLength = baseRamp.toInt
  val httpProtocol = http.baseURL(baseURL).inferHtmlResources()

  val negotiation = scenario("SLATemplate retrieve for " + baseUser + " users on ramp of" + baseRamp).exec(
	 SLOManager.retrieveRandomTemplate
  )
  setUp(negotiation.inject(constantUsersPerSec(numUsers) during(rampLength seconds)).protocols(httpProtocol))
}
